package tech.iooo.vertx;

import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.iooo.vertx.redis.RedisCluster;
import tech.iooo.vertx.redis.RedisClusterOptions;

/**
 * Hello world!
 *
 * @author Ivan97
 */
public class RedisClusterApplication {

  private static final Logger logger = LoggerFactory.getLogger(RedisClusterApplication.class);

  public static void main(String[] args) {
    logger.info("Hello world");
    RedisCluster redisCluster = RedisCluster.create(Vertx.vertx(), new RedisClusterOptions());
  }
}
