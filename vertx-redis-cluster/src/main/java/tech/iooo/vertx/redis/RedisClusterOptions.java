package tech.iooo.vertx.redis;

import io.vertx.redis.RedisOptions;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 2018/8/22 下午3:48
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-vertx-extension-bundle">Ivan97</a>
 */
public class RedisClusterOptions {
  private List<RedisConfigMetadata> nodes;
  private RedisOptions redisOptions;

  public RedisClusterOptions() {
    this.nodes = new ArrayList<>();
    this.redisOptions = new RedisOptions();
  }

  public RedisClusterOptions addNode(RedisConfigMetadata redisConfigMetadata) {
    nodes.add(redisConfigMetadata);
    return this;
  }

  public RedisConfigMetadata getNode(int index) {
    return nodes.get(index);
  }

  public RedisOptions getRedisOptions() {
    return redisOptions;
  }

  public RedisOptions cloneRedisOptions() {
    return new RedisOptions().setAddress(redisOptions.getAddress())
        .setAuth(redisOptions.getAuth())
        .setEncoding(redisOptions.getEncoding())
        .setSelect(redisOptions.getSelect())
        .setBinary(redisOptions.isBinary());
  }

  public void setRedisOptions(RedisOptions redisOptions) {
    this.redisOptions = redisOptions;
  }

  public List<RedisConfigMetadata> getNodes() {
    return nodes;
  }

  public void setNodes(List<RedisConfigMetadata> nodes) {
    this.nodes = nodes;
  }

  public int size() {
    return nodes.size();
  }

  public RedisOptions getRedisOptions(int index) {
    return new RedisOptions().setAddress(redisOptions.getAddress())
        .setAuth(redisOptions.getAuth())
        .setEncoding(redisOptions.getEncoding())
        .setSelect(redisOptions.getSelect())
        .setBinary(redisOptions.isBinary())
        .setHost(nodes.get(index).getHost())
        .setPort(nodes.get(index).getPort());
  }
}
