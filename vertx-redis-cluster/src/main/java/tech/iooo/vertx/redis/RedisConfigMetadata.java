package tech.iooo.vertx.redis;

import lombok.Data;

/**
 * Created on 2018/8/22 下午3:49
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-vertx-extension-bundle">Ivan97</a>
 */
@Data
public class RedisConfigMetadata {

  private String host;
  private Integer port;
}
