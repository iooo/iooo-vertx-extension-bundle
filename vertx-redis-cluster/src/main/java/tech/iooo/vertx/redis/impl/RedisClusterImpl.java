package tech.iooo.vertx.redis.impl;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.redis.op.InsertOptions;
import io.vertx.redis.op.LimitOptions;
import io.vertx.redis.op.RangeLimitOptions;
import io.vertx.redis.op.RangeOptions;
import io.vertx.redis.op.SetOptions;
import java.util.List;
import java.util.Map;
import tech.iooo.vertx.redis.RedisCluster;
import tech.iooo.vertx.redis.RedisClusterOptions;

/**
 * Created on 2018/8/22 下午3:50
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-vertx-extension-bundle">Ivan97</a>
 */
public class RedisClusterImpl extends AbstractRedisClusterClient {

  public RedisClusterImpl(Vertx vertx, RedisClusterOptions config) {
    super(vertx, config);
  }

  @Override
  public void close(Handler<AsyncResult<Void>> handler) {

  }

  @Override
  public RedisCluster append(String key, String value, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster blpop(String key, int seconds, Handler<AsyncResult<JsonArray>> handler) {
    return null;
  }

  @Override
  public RedisCluster brpop(String key, int seconds, Handler<AsyncResult<JsonArray>> handler) {
    return null;
  }

  @Override
  public RedisCluster brpoplpush(String key, String destkey, int seconds, Handler<AsyncResult<String>> handler) {
    return null;
  }

  @Override
  public RedisCluster decr(String key, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster decrby(String key, long decrement, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster del(String key, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster exists(String key, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster expire(String key, int seconds, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster expireat(String key, long seconds, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster get(String key, Handler<AsyncResult<String>> handler) {
    return null;
  }

  @Override
  public RedisCluster getBinary(String key, Handler<AsyncResult<Buffer>> handler) {
    return null;
  }

  @Override
  public RedisCluster getbit(String key, long offset, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster getrange(String key, long start, long end, Handler<AsyncResult<String>> handler) {
    return null;
  }

  @Override
  public RedisCluster getset(String key, String value, Handler<AsyncResult<String>> handler) {
    return null;
  }

  @Override
  public RedisCluster hdel(String key, String field, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster hexists(String key, String field, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster hget(String key, String field, Handler<AsyncResult<String>> handler) {
    return null;
  }

  @Override
  public RedisCluster hgetall(String key, Handler<AsyncResult<JsonObject>> handler) {
    return null;
  }

  @Override
  public RedisCluster hincrby(String key, String field, long increment, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster hincrbyfloat(String key, String field, double increment, Handler<AsyncResult<String>> handler) {
    return null;
  }

  @Override
  public RedisCluster hkeys(String key, Handler<AsyncResult<JsonArray>> handler) {
    return null;
  }

  @Override
  public RedisCluster hlen(String key, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster hmget(String key, List<String> fields, Handler<AsyncResult<JsonArray>> handler) {
    return null;
  }

  @Override
  public RedisCluster hmset(String key, JsonObject values, Handler<AsyncResult<String>> handler) {
    return null;
  }

  @Override
  public RedisCluster hset(String key, String field, String value, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster hsetnx(String key, String field, String value, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster hvals(String key, Handler<AsyncResult<JsonArray>> handler) {
    return null;
  }

  @Override
  public RedisCluster incr(String key, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster incrby(String key, long increment, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster incrbyfloat(String key, double increment, Handler<AsyncResult<String>> handler) {
    return null;
  }

  @Override
  public RedisCluster lindex(String key, int index, Handler<AsyncResult<String>> handler) {
    return null;
  }

  @Override
  public RedisCluster linsert(String key, InsertOptions option, String pivot, String value,
      Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster llen(String key, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster lpop(String key, Handler<AsyncResult<String>> handler) {
    return null;
  }

  @Override
  public RedisCluster lpushMany(String key, List<String> values, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster lpush(String key, String value, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster lpushx(String key, String value, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster lrange(String key, long from, long to, Handler<AsyncResult<JsonArray>> handler) {
    return null;
  }

  @Override
  public RedisCluster lrem(String key, long count, String value, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster lset(String key, long index, String value, Handler<AsyncResult<String>> handler) {
    return null;
  }

  @Override
  public RedisCluster ltrim(String key, long from, long to, Handler<AsyncResult<String>> handler) {
    return null;
  }

  @Override
  public RedisCluster mget(String key, Handler<AsyncResult<JsonArray>> handler) {
    return null;
  }

  @Override
  public RedisCluster rpop(String key, Handler<AsyncResult<String>> handler) {
    return null;
  }

  @Override
  public RedisCluster rpoplpush(String key, String destkey, Handler<AsyncResult<String>> handler) {
    return null;
  }

  @Override
  public RedisCluster rpushMany(String key, List<String> values, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster rpush(String key, String value, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster rpushx(String key, String value, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster sadd(String key, String member, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster saddMany(String key, List<String> members, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster scard(String key, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster set(String key, String value, Handler<AsyncResult<Void>> handler) {
    return null;
  }

  @Override
  public RedisCluster setWithOptions(String key, String value, SetOptions options,
      Handler<AsyncResult<String>> handler) {
    return null;
  }

  @Override
  public RedisCluster setex(String key, long seconds, String value, Handler<AsyncResult<String>> handler) {
    return null;
  }

  @Override
  public RedisCluster setnx(String key, String value, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster setrange(String key, int offset, String value, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster sismember(String key, String member, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster smembers(String key, Handler<AsyncResult<JsonArray>> handler) {
    return null;
  }

  @Override
  public RedisCluster spop(String key, Handler<AsyncResult<String>> handler) {
    return null;
  }

  @Override
  public RedisCluster srem(String key, String member, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster sremMany(String key, List<String> members, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster strlen(String key, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster ttl(String key, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster type(String key, Handler<AsyncResult<String>> handler) {
    return null;
  }

  @Override
  public RedisCluster zadd(String key, double score, String member, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster zaddMany(String key, Map<String, Double> members, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster zcard(String key, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster zcount(String key, double min, double max, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster zincrby(String key, double increment, String member, Handler<AsyncResult<String>> handler) {
    return null;
  }

  @Override
  public RedisCluster zlexcount(String key, String min, String max, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster zrange(String key, long start, long stop, Handler<AsyncResult<JsonArray>> handler) {
    return null;
  }

  @Override
  public RedisCluster zrangeWithOptions(String key, long start, long stop, RangeOptions options,
      Handler<AsyncResult<JsonArray>> handler) {
    return null;
  }

  @Override
  public RedisCluster zrangebylex(String key, String min, String max, LimitOptions options,
      Handler<AsyncResult<JsonArray>> handler) {
    return null;
  }

  @Override
  public RedisCluster zrangebyscore(String key, String min, String max, RangeLimitOptions options,
      Handler<AsyncResult<JsonArray>> handler) {
    return null;
  }

  @Override
  public RedisCluster zrank(String key, String member, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster zrem(String key, String member, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster zremMany(String key, List<String> members, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster zremrangebylex(String key, String min, String max, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster zremrangebyrank(String key, long start, long stop, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster zremrangebyscore(String key, String min, String max, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster zrevrange(String key, long start, long stop, RangeOptions options,
      Handler<AsyncResult<JsonArray>> handler) {
    return null;
  }

  @Override
  public RedisCluster zrevrangebylex(String key, String max, String min, LimitOptions options,
      Handler<AsyncResult<JsonArray>> handler) {
    return null;
  }

  @Override
  public RedisCluster zrevrangebyscore(String key, String max, String min, RangeLimitOptions options,
      Handler<AsyncResult<JsonArray>> handler) {
    return null;
  }

  @Override
  public RedisCluster zrevrank(String key, String member, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster zscore(String key, String member, Handler<AsyncResult<String>> handler) {
    return null;
  }

  @Override
  public RedisCluster hget(byte[] key, byte[] field, Handler<AsyncResult<String>> handler) {
    return null;
  }

  @Override
  public RedisCluster hmget(byte[] key, List<byte[]> fields, Handler<AsyncResult<JsonArray>> handler) {
    return null;
  }

  @Override
  public RedisCluster smembers(byte[] key, Handler<AsyncResult<JsonArray>> handler) {
    return null;
  }

  @Override
  public RedisCluster sismember(byte[] key, byte[] member, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster hincrby(byte[] key, byte[] field, long increment, Handler<AsyncResult<String>> handler) {
    return null;
  }

  @Override
  public RedisCluster zrange(byte[] key, long start, long stop, Handler<AsyncResult<JsonArray>> handler) {
    return null;
  }

  @Override
  public RedisCluster zadd(byte[] key, double score, byte[] member, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster zremrangebyscore(byte[] key, double start, double end,
      Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster scard(byte[] key, Handler<AsyncResult<Long>> handler) {
    return null;
  }

  @Override
  public RedisCluster zrangebyscore(byte[] key, double min, double max, RangeLimitOptions options,
      Handler<AsyncResult<JsonArray>> handler) {
    return null;
  }

  @Override
  public RedisCluster expire(byte[] key, int seconds, Handler<AsyncResult<String>> handler) {
    return null;
  }
}
