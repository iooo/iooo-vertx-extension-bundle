package tech.iooo.vertx.redis.exception;

/**
 * Created on 2018/8/22 下午3:58
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-vertx-extension-bundle">Ivan97</a>
 */
public class RedisClusterException extends RuntimeException {

  public RedisClusterException(String message) {
    super(message);
  }

  public RedisClusterException(Throwable e) {
    super(e);
  }

  public RedisClusterException(String message, Throwable cause) {
    super(message, cause);
  }
}
