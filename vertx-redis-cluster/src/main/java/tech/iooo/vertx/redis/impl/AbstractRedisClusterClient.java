package tech.iooo.vertx.redis.impl;

import io.vertx.core.Vertx;
import java.nio.charset.Charset;
import tech.iooo.vertx.redis.RedisCluster;
import tech.iooo.vertx.redis.RedisClusterOptions;

/**
 * Created on 2018/8/22 下午3:50
 *
 * @author <a href="mailto:yangkizhang@gmail.com?subject=iooo-vertx-extension-bundle">Ivan97</a>
 */
public abstract class AbstractRedisClusterClient implements RedisCluster {

  private final Vertx vertx;
  private final String encoding;
  private final Charset charset;
  private final Charset binaryCharset;
  private final String baseAddress;

  AbstractRedisClusterClient(Vertx vertx, RedisClusterOptions config) {
    this.vertx = vertx;
    this.encoding = config.getRedisOptions().getEncoding();
    this.charset = Charset.forName(encoding);
    this.binaryCharset = Charset.forName("iso-8859-1");
    this.baseAddress = config.getRedisOptions().getAddress();
  }
}
